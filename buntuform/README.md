# buntuform

Folder keeps track of base work with Ubuntu to install on KVM via Terraform. The goal is to take the base work from this project and expand to clusters of similar systems in the future. 

## How to use

1. Ensure cloud init and networking folders are made as desired. 
1. Make sure `ubuntu-example.tf` is set up correctly with storage and other details.
1. Run in the folder `terraform init`. If that fails, see previous folder for Go dependencies. 
1. Run in the folder `terraform plan`. 
1. Finally, run `terraform apply`. View and interract with the new VM in virt-manager. 
