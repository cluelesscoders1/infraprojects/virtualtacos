provider "linode" {
    token = ""
}

resource "linode_instance" "test_insta" {
    label = "test_Linode_1"
    image = "linode/centos7"
    group = "Terraform"
    region = "us-east"
    type =  "g6-standard-1"
    root_pass = "This_is_passw0rd"
}