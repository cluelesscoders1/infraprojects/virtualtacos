# VirtualTacos

KVM project on a local machine. Working towards deploying k3s clusters with KVM.

## Pre-req KVMs

After system pre-reqs are installed. 
* `go get github.com/dmacvicar/terraform-provider-libvirt`
* `go install github.com/dmacvicar/terraform-provider-libvirt`

## Resources
Guides: 
* [Computing geeks KVM](https://computingforgeeks.com/install-kvm-centos-rhel-ubuntu-debian-sles-arch/)
* [Linode Requirements](https://www.terraform.io/docs/providers/linode/index.html)
* [Linode Terraform Basics](https://www.linode.com/docs/applications/configuration-management/how-to-build-your-infrastructure-using-terraform-and-linode/)
